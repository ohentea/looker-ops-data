connection: "p2f-analytics-bq"
label: "Operations Data"

include: "/views/*.view"

explore: salesforce_opportunity  {
  join: salesforce_account {
    relationship: many_to_one
    sql_on: ${salesforce_opportunity.account_id} = ${salesforce_account.account_id} ;;
  }
}

explore: salesforce_account  {
  join:  salesforce_opportunity {
    relationship: one_to_many
    sql_on: ${salesforce_opportunity.account_id} = ${salesforce_account.account_id} ;;
  }
  join: salesforce_task {
    relationship: one_to_many
    sql_on: ${salesforce_account.account_id} = ${salesforce_task.account_id} ;;
  }
}

explore: salesforce_product {}

explore: pingdom_checks {}

explore: bamboohr {
  label: "BambooHR"
}
