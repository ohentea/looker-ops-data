view: salesforce_opportunity {
  sql_table_name: `hevo_dataset_ptf_operations_analytics_0SNh.salesforce_sourcedata_opportunity`  ;;
  # Data dictionary: https://developer.salesforce.com/docs/atlas.en-us.object_reference.meta/object_reference/sforce_api_objects_opportunity.htm

# Define your dimensions and measures here, like this:
  dimension: opportunity_id {
    primary_key: yes
    description: "Unique ID for each opportunity listed. Also links to Salesforce page"
    type: string
    sql: ${TABLE}.id ;;
    link: {
      label: "Salesforce Opportunity"
      url: "https://d36000000ilhkeas.lightning.force.com/lightning/r/Opportunity/{{ value }}/view"
    }
  }

  dimension: opportunity_type {
    type: string
    description: "New, Renewal, Up-Sell"
    suggestions: ["New-Logo", "Renewal-New", "Up-Sell", "Existing Logo-New Division", "Cross-Sell", "Renewal-Upsell"]
    sql: ${TABLE}.opportunity_type__c ;;
  }

  dimension: opportunity_type_grouped {
    type:  string
    case: {
      when: {
        sql: ${opportunity_type} IN ("New-Logo", "Existing Logo-New Division") ;;
        label: "New"
      }
      when: {
        sql: ${opportunity_type} IN ("Renewal","Renewal-Upsell","Renewal-New","Cross-Sell","Up-Sell") ;;
        label: "Renewal"
      }
      else: "Other"
    }
    description: "Groups opportunity type into New and Renewal (including Up-sells Cross-sells)"
  }


  dimension: account_id {
    type: string
    sql: ${TABLE}.accountid ;;
    hidden: yes
  }

  dimension: owner_id {
    type: string
    sql: ${TABLE}.ownerid ;;
    hidden: yes
  }

  dimension: opportunity_name {
    type: string
    description: ""
    sql: ${TABLE}.name ;;
  }

  dimension: opportunity_amount {
    type: number
    sql:  ${TABLE}.amount ;;
    description: "Amount for this specific opportunity. Use Sum or Average  measure to aggregate"
  }

  dimension: stage_name_detailed {
    type: string
    sql:${TABLE}.stagename ;;
  }

  dimension: committed {
    type: string
    case: {
      when: {
        sql: ${stage_name_detailed} = 'Closed - Won' ;;
        label: "Committed"
      }
      else: "In Pipeline"
    }
    description: "Returns Committed if stage name = Closed - Won"
  }

  # dimension: department {
  #   type: string
  #   sql: ${TABLE}.sub_department ;;
  # }

  # dimension: department_grouped {
  #   type: string
  #   case: {
  #     when: {
  #       sql: ${department} LIKE '%Marketing:%' ;;
  #       label: "Marketing"
  #     }
  #     when: {
  #       sql: ${department} LIKE '%Success:%' ;;
  #       label: "Customer Success"
  #     }
  #     when: {
  #       sql: ${department} LIKE '%DEI%' ;;
  #       label: "DEI"
  #     }
  #     when: {
  #       sql: ${department} LIKE '%Staff Aug%' ;;
  #       label: "Staff Aug"
  #     }
  #     else: "Other"
  #   }
  # }

  ## DATES ##

  dimension_group: contract_start {
    type: time
    description: ""
    timeframes: [
      date,
      week,
      month,
      quarter
    ]
    convert_tz: no
    group_label: "Dates"
    datatype: date
    sql: ${TABLE}.contract_start_date__c ;;
  }

  dimension_group: contract_end {
    type: time
    description: ""
    timeframes: [
      date,
      week,
      month,
      quarter
    ]
    convert_tz: no
    group_label: "Dates"
    datatype: date
    sql: ${TABLE}.contract_end_date__c ;;
  }

  dimension_group: contract_close {
    type: time
    description: "Date when the opportunity is expected to close."
    timeframes: [
      date,
      week,
      month,
      quarter
    ]
    convert_tz: no
    group_label: "Dates"
    datatype: date
    sql: ${TABLE}.closedate ;;
  }

  dimension_group: created {
    type: time
    description: "Date when the opportunity was  created."
    timeframes: [date]
    convert_tz: no
    group_label: "Dates"
    datatype: timestamp
    sql: ${TABLE}.createddate ;;
  }

  dimension: comittment_term {
    type: string
    description: ""
    sql: ${TABLE}.comittment_term__c ;;
  }

  dimension: concierge_service {
    type: yesno
    description: ""
    group_label: "Services"
    sql: ${TABLE}.concierge_service__c ;;
  }


  dimension: dedicated_event {
    type: yesno
    description: ""
    group_label: "Services"
    sql: ${TABLE}.dedicated_event__c ;;
  }

  dimension: dedicated_sourcing {
    type: yesno
    description: ""
    group_label: "Services"
    sql: ${TABLE}.dedicated_sourcing__c ;;
  }

  dimension: dedicated_sourcing_type {
    type: string
    description: "Full-time or part-time"
    group_label: "Services"
    sql: ${TABLE}.dedicated_sourcing_type__c ;;
  }

  dimension: dei_consulting {
    label: "DEI Consulting"
    type: yesno
    description: ""
    group_label: "Services"
    sql: ${TABLE}.dei_consulting__c ;;
  }

  dimension: dei_keynote {
    label: "DEI Keynote"
    type: yesno
    description: ""
    group_label: "Services"
    sql: ${TABLE}.dei_keynote__c ;;
  }

  dimension: dei_listening_circles {
    label: "DEI Listening Circles"
    type: yesno
    description: ""
    group_label: "Services"
    sql: ${TABLE}.dei_listening_circles__c ;;
  }

  dimension: dei_training {
    label: "DEI Training"
    type: yesno
    description: ""
    group_label: "Services"
    sql: ${TABLE}.dei_training__c ;;
  }

  dimension: description_of_services {
    type: string
    description: ""
    group_label: "Services"
    hidden: yes
    sql: ${TABLE}.description_of_services__c ;;
  }

  dimension: enterprise_gold_15k {
    type: yesno
    description: ""
    group_label: "Services"
    sql: ${TABLE}.enterprise_gold_15k__c ;;
  }

  dimension: enterprise_platinum_20k {
    type: yesno
    description: ""
    group_label: "Services"
    sql: ${TABLE}.enterprise_platinum_20k__c ;;
  }

  dimension: enterprise_silver_10k {
    type: yesno
    description: ""
    group_label: "Services"
    sql: ${TABLE}.enterprise_silver_10k__c ;;
  }


  ## CHURN ##
  dimension: churn_reason {
    type: string
    description: ""
    group_label: "Churn"
    sql: ${TABLE}.churn_reason__c ;;
  }

  dimension: churn_reason_ae_comment {
    type: string
    description: ""
    group_label: "Churn"
    sql: ${TABLE}.churn_reason_ae_comment__c ;;
  }

  dimension_group: churned_date {
    type: time
    description: ""
    group_label: "Churn"
    timeframes: [
      date,
      week,
      month,
      quarter
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.churned_date__c ;;
  }

##############
## MEASURES ##
##############

  measure: count_opportunities {
    type: count_distinct
    sql: ${opportunity_id} ;;
  }

  measure: total_amount {
    type: sum
    description: "Estimated total sale amount. For opportunities with products, the amount is the sum of the related products."
    value_format: "[>=1000000]$0.0,,\"M\";[>=1000]$0,\"K\";$0"
    sql: ${opportunity_amount} ;;
    drill_fields: []
  }

  measure: average_amount {
    type: average
    value_format: "[>=1000000]$0.0,,\"M\";[>=1000]$0,\"K\";$0"
    sql: ${opportunity_amount} ;;
    drill_fields: []

  }

  measure: amount_dei_consulting {
    type: sum
    description: ""
    label: "Amount DEI Consulting"
    group_label: "Services"
    group_item_label: "Amount DEI Consulting"
    value_format: "[>=1000000]$0.0,,\"M\";[>=1000]$0,\"K\";$0"
    sql: ${TABLE}.amount_dei_consulting__c ;;
  }

  measure: amount_dei_keynotes {
    type: sum
    description: ""
    label: "Amount DEI Keynotes"
    group_label: "Services"
    group_item_label: "Amount DEI Keynotes"
    value_format: "[>=1000000]$0.0,,\"M\";[>=1000]$0,\"K\";$0"
    sql: ${TABLE}.amount_dei_keynotes__c ;;
  }

  measure: amount_dei_listening_circles {
    type: sum
    description: ""
    label: "Amount DEI Listening Circles"
    group_label: "Services"
    group_item_label: "Amount DEI Listening Circles"
    value_format: "[>=1000000]$0.0,,\"M\";[>=1000]$0,\"K\";$0"
    sql: ${TABLE}.amount_dei_listening_circles__c ;;
  }

  measure: amount_fairs {
    type: sum
    description: ""
    group_label: "Services"
    value_format: "[>=1000000]$0.0,,\"M\";[>=1000]$0,\"K\";$0"
    sql: ${TABLE}.amount_fairs__c ;;
  }

  measure: amount_mentorship {
    type: sum
    description: ""
    group_label: "Services"
    value_format: "[>=1000000]$0.0,,\"M\";[>=1000]$0,\"K\";$0"
    sql: ${TABLE}.amount_mentorship__c ;;
  }

  measure: amount_ds {
    type: sum
    description: ""
    label: "Amount DS"
    group_label: "Services"
    value_format: "[>=1000000]$0.0,,\"M\";[>=1000]$0,\"K\";$0"
    sql: ${TABLE}.amount_of_ds__c ;;
  }

  measure: amount_sourcing_credits {
    type: sum
    description: ""
    group_label: "Services"
    value_format: "[>=1000000]$0.0,,\"M\";[>=1000]$0,\"K\";$0"
    sql: ${TABLE}.amount_of_sourcing_credits__c ;;
  }

  measure: amount_rjb {
    type: sum
    description: ""
    label: "Amount RJB"
    group_label: "Services"
    value_format: "[>=1000000]$0.0,,\"M\";[>=1000]$0,\"K\";$0"
    sql: ${TABLE}.amount_rjb__c ;;
  }

  measure: amount_staf_augmentation {
    type: sum
    description: ""
    group_label: "Services"
    value_format: "[>=1000000]$0.0,,\"M\";[>=1000]$0,\"K\";$0"
    sql: ${TABLE}.amount_staf_aug__c ;;
  }

  measure: amount_summit_sponsorship {
    type: sum
    description: ""
    group_label: "Services"
    value_format: "[>=1000000]$0.0,,\"M\";[>=1000]$0,\"K\";$0"
    sql: ${TABLE}.amount_summit_sponsorship__c ;;
  }


  measure: count_circles {
    type: sum
    description: ""
    group_label: "Services"
    sql: ${TABLE}.number_of_circles__c ;;
  }

  measure: count_credits {
    type: sum
    description: ""
    group_label: "Services"
    sql: ${TABLE}.number_of_credits__c ;;
  }

  measure: count_events {
    type: sum
    description: ""
    group_label: "Services"
    sql: ${TABLE}.number_of_events__c ;;
  }

  measure: count_fairs {
    type: sum
    description: ""
    group_label: "Services"
    sql: ${TABLE}.number_of_fairs__c ;;
  }

  measure: count_freelancers {
    type: sum
    description: ""
    group_label: "Services"
    sql: ${TABLE}.number_of_freelancers__c ;;
  }

  measure: count_hackathons {
    type: sum
    description: ""
    group_label: "Services"
    sql: ${TABLE}.number_of_hackathons__c ;;
  }

  measure: count_hours {
    type: sum
    description: ""
    group_label: "Services"
    sql: ${TABLE}.number_of_hours__c ;;
  }

  measure: count_hours_dei {
    label: "Number of Hours DEI"
    type: sum
    description: ""
    group_label: "Services"
    sql: ${TABLE}.number_of_hours_dei__c ;;
  }

  measure: count_keynotes {
    type: sum
    description: ""
    group_label: "Services"
    sql: ${TABLE}.number_of_keynotes__c ;;
  }

  measure: count_mentors {
    type: sum
    description: ""
    group_label: "Services"
    sql: ${TABLE}.number_of_mentors__c ;;
  }

  measure: count_pieces {
    type: sum
    description: ""
    group_label: "Services"
    sql: ${TABLE}.number_of_pieces__c ;;
  }

  measure: count_roles {
    type: sum
    description: ""
    group_label: "Services"
    sql: ${TABLE}.number_of_roles__c ;;
  }

  measure: count_sessions_dei {
    label: "Number of Sessions DEI"
    type: sum
    description: ""
    group_label: "Services"
    sql: ${TABLE}.number_of_sessions_dei__c ;;
  }

}
