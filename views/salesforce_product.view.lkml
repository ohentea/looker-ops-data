view: salesforce_product {
  sql_table_name: `hevo_dataset_ptf_operations_analytics_0SNh.salesforce_sourcedata_product2`
    ;;
  drill_fields: [product_id]

  ## SF reference https://developer.salesforce.com/docs/atlas.en-us.object_reference.meta/object_reference/sforce_api_objects_product2.htm

  dimension: product_id {
    primary_key: yes
    type: string
    description: ""
    sql: ${TABLE}.id ;;
    link: {
      label: "Salesforce Product"
      url: "https://d36000000ilhkeas.lightning.force.com/lightning/r/Product2/{{ value }}/view"
    }
  }

  dimension: product_name {
    type: string
    description: ""
    suggestions: ["Job Scraper", "Virtual Job Fair", "Staff Augmentation", "Dedicated Event", "Dedicated Sourcing"]
    sql: ${TABLE}.name ;;link: {
      label: "Salesforce Product"
      url: "https://d36000000ilhkeas.lightning.force.com/lightning/r/Product2/{{ salesforce_product.product_id._value }}/view"
    }
  }

  dimension: description {
    type: string
    description: "A text description of this product"
    sql: ${TABLE}.description ;;
  }

  dimension: family {
    type: string
    description: "Name of the product family associated with this product"
    suggestions: ["Attraction", "DEI Services", "Add-ons", "Acquisition", "Hire", "Grow", "PowerPro", "Retain", "PowerUp"]
    sql: ${TABLE}.family ;;
  }

  dimension: is_active {
    type: yesno
    description: ""
    sql: ${TABLE}.isactive ;;
  }

  dimension: product_code {
    type: string
    description: "Internal code"
    sql: ${TABLE}.productcode ;;
  }
  dimension: product_selling_term {
    type: string
    description: "One-time or annual"
    suggestions: ["Annual", "One time"]
    sql: ${TABLE}.product_selling_term__c ;;
  }
  dimension: product_revenue_type {
    type: string
    description: "Recurring or one-time"
    suggestions: ["Recurring","Non-recurring"]
    sql: ${TABLE}.revenue_type__c ;;
  }

  dimension: category {
    type: string
    description: "Core, Sourcing, DEIB, Other"
    suggestions: ["Core Product", "Sourcing Platform", "Other", "DEIB Leadership Academy"]
    sql: ${TABLE}.category__c ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }

}
