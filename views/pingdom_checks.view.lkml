view: pingdom_checks {
  sql_table_name: `ptf-operations-analytics.hevo_dataset_ptf_operations_analytics_0SNh.pingdom_checks_total_api`
    ;;
  drill_fields: [check_id]

  ## API checks https://docs.pingdom.com/api/#tag/Checks/paths/~1checks/get
  ## Hevo pipeline https://us.hevodata.com/pipeline/59

  dimension: check_id {
    primary_key: yes
    type: number
    description: "Unique identifier for check. There is one check per source"
    sql: ${TABLE}.id ;;
  }

  # dimension: __hevo__ingested_at {
  #   type: number
  #   sql: ${TABLE}.__hevo__ingested_at ;;
  # }

  # dimension: __hevo__loaded_at {
  #   type: number
  #   sql: ${TABLE}.__hevo__loaded_at ;;
  # }

  dimension_group: created {
    type: time
    timeframes: [date]
    sql: TIMESTAMP_SECONDS(${TABLE}.created) ;;
  }

  dimension: hostname {
    type: string
    description: "URL being checked"
    suggestions: ["powertofly.com","www.powertofly.com","assets.powertofly.com","blog.powertofly.com"]
    sql: ${TABLE}.hostname ;;
  }

  dimension: ipv6 {
    type: yesno
    sql: ${TABLE}.ipv6 ;;
  }

  dimension_group: last_down_end {
    type: time
    timeframes: [date]
    sql: TIMESTAMP_SECONDS(${TABLE}.lastdownend) ;;
  }

  dimension_group: last_down_start {
    type: time
    timeframes: [time, date]
    sql: TIMESTAMP_SECONDS(${TABLE}.lastdownstart) ;;
  }

  dimension_group: last_error {
    type: time
    timeframes: [time, date]
    sql: TIMESTAMP_SECONDS(${TABLE}.lasterrortime) ;;
  }

  dimension_group: last_response {
    type: time
    timeframes: [time, date]
    sql: TIMESTAMP_SECONDS(${TABLE}.lastresponsetime) ;;
  }

  dimension_group: last_test {
    type: time
    timeframes: [time, date]
    sql: TIMESTAMP_SECONDS(${TABLE}.lasttesttime) ;;
  }

  dimension: check_name {
    type: string
    description: "Name of website checked (e.g. Landing, Executive Forum, PTF)"
    sql: ${TABLE}.name ;;
  }

  dimension: resolution {
    type: number
    sql: ${TABLE}.resolution ;;
  }

  dimension: status {
    type: string
    description: "Whether check is up or not"
    suggestions: ["up"]
    sql: ${TABLE}.status ;;
  }

  dimension: type {
    type: string
    description: "Normally HTTP"
    sql: ${TABLE}.type ;;
  }

  dimension: verify_certificate {
    type: yesno
    sql: ${TABLE}.verify_certificate ;;
  }

  measure: count {
    type: count
    drill_fields: [check_id, hostname, check_name]
  }
}
