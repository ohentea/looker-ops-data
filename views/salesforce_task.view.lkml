view: salesforce_task {
  sql_table_name: `ptf-operations-analytics.hevo_dataset_ptf_operations_analytics_0SNh.salesforce_sourcedata_task`
    ;;
  drill_fields: []

  dimension: task_id {
    primary_key: yes
    type: string
    description: "Unique identifier for each task"
    sql: ${TABLE}.id ;;
    link: {
      label: "Salesforce Task"
      url: "https://d36000000ilhkeas.lightning.force.com/lightning/o/Task/list?filterName={{ value }}"
    }
  }

  dimension: account_id {
    type: string
    hidden: yes
    description: ""
    sql: ${TABLE}.accountid ;;
  }
  dimension: created_by_id {
    type: string
    hidden: yes
    description: ""
    sql: ${TABLE}.createdbyid ;;
  }

  dimension: owner_id {
    type: string
    hidden: yes
    description: "{\"dl\":\"512\"}"
    sql: ${TABLE}.ownerid ;;
  }

  dimension: task_name {
    type: string
    description: "The subject line of the task, such as “Call” or “Send Quote.” Limit: 255 characters."
    sql: ${TABLE}.subject ;;
    link: {
      label: "Salesforce Task"
      url: "https://d36000000ilhkeas.lightning.force.com/lightning/o/Task/list?filterName={ salesforce_task.task_id._value }}"
    }
  }

  dimension: activity_count {
    type: number
    description: ""
    sql: ${TABLE}.activity_count__c ;;
  }

  dimension: task_status {
    type: string
    description: "Completed, Open, No Show"
    suggestions: ["Completed", "Open", "No Show", "Not Started"]
    sql: ${TABLE}.status ;;
  }

  # dimension_group: activity_custom {
  #   type: time
  #   description: ""
  #   timeframes: [
  #     date,
  #     week,
  #     month
  #   ]
  #   convert_tz: no
  #   datatype: date
  #   sql: ${TABLE}.activity_date__c ;;
  # }

  dimension_group: activity {
    type: time
    description: ""
    timeframes: [
      date,
      week,
      month
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.activitydate ;;
  }

  dimension: call_count {
    type: number
    description: ""
    group_label: "Call info"
    sql: ${TABLE}.call_count__c ;;
  }

  dimension: call_type {
    type: string
    description: "Such as First Call, Follow-up Call, etc."
    group_label: "Call info"
    suggestions: ["First call-Outbound/SMB", "First call-Inbound/SMB", "Follow-up Call", "First call-Outbound/MMKT", "First call-Outbound/ENT", "First call-Inbound/MMKT", "First call-Inbound/ENT", "First Call"]
    sql: ${TABLE}.call_type__c ;;
  }

  dimension: call_disposition {
    type: string
    description: "Result of call, such as no answer, connected, wrong number"
    group_label: "Call info"
    suggestions: ["No Answer", "Left Voicemail", "Gatekeeper", "Bad Number", "No Number", "Connected", "Not in Service", "Wrong Number", "Answered - Not Interested", "Busy"]
    sql: ${TABLE}.calldisposition ;;
  }

  # dimension: calldurationinseconds {
  #   type: number
  #   description: "{\"dl\":\"4\"}"
  #   sql: ${TABLE}.calldurationinseconds ;;
  # }

  dimension: call_inbound_outbound {
    type: string
    description: "Direction of call, vast majority outbound"
    group_label: "Call info"
    sql: ${TABLE}.calltype ;;
  }

  dimension_group: completed {
    type: time
    description: "{\"dl\":\"13\"}"
    timeframes: [
      time,
      date,
      week,
      month,
      quarter
    ]
    sql: ${TABLE}.completeddatetime ;;
  }

  dimension: created_by_role {
    type: string
    suggestions: ["System Administrator", "BDR", "RJB AE", "Sales Rep", "SMB AE", "ENT AE", "EMEA AE", "MMKT AE", "Executives"]
    description: "The role of the user who creatd the task"
    sql: ${TABLE}.created_by_role__c ;;
  }

  dimension_group: created {
    type: time
    description: "When the task was first created"
    timeframes: [
      time,
      date,
      week,
      month,
      quarter
    ]
    sql: ${TABLE}.createddate ;;
  }

  dimension: description {
    type: string
    description: "Returns first 200 characters of description. Go to Salesforce page for further detail"
    sql: left(${TABLE}.description, 200) ;;
  }

  dimension: email {
    type: yesno
    description: "Returns yes if email count higher than 0"
    sql: ${TABLE}.email_count__c > 0 ;;
  }

  dimension: is_archived {
    type: yesno
    description: "Indicates whether the event has been archived. The default value of this field is false."
    sql: ${TABLE}.isarchived ;;
  }

  dimension: is_closed {
    type: yesno
    description: "Indicates whether the task has been completed (true) or not (false). The default value of this field is false"
    sql: ${TABLE}.isclosed ;;
  }

  # dimension: is_deleted {
  #   type: yesno
  #   description: "{\"dl\":\"5\"}"
  #   sql: ${TABLE}.isdeleted ;;
  # }

  # dimension: is_high_priority {
  #   type: yesno
  #   description: "Indicates a high-priority task. This field is derived from the Priority field. The default value of this field is false."
  #   sql: ${TABLE}.ishighpriority ;;
  # }

  # dimension: is_recurrence {
  #   type: yesno
  #   description: "{\"dl\":\"5\"}"
  #   sql: ${TABLE}.isrecurrence ;;
  # }

  # dimension: is_reminder_set {
  #   type: yesno
  #   description: "Indicates whether a popup reminder has been set for the task (true) or not (false). The default value of this field is false."
  #   sql: ${TABLE}.isreminderset ;;
  # }

  # dimension: last_modified_by_id {
  #   type: string
  #   description: "{\"dl\":\"512\"}"
  #   sql: ${TABLE}.lastmodifiedbyid ;;
  # }

  dimension_group: last_modified {
    type: time
    description: ""
    timeframes: [
      time
    ]
    sql: ${TABLE}.lastmodifieddate ;;
  }

  dimension: task_priority {
    type: string
    description: "Indicates the importance or urgency of a task, such as high or low. The default value of this field is Normal."
    suggestions: ["Normal", "Low", "High", "Not Started"]
    sql: ${TABLE}.priority ;;
  }

  dimension: task_subtype {
    type: string
    description: "Provides standard subtypes to facilitate creating and searching for specific task subtypes. "
    sql: ${TABLE}.tasksubtype ;;
  }

  dimension: task_type {
    type: string
    description: "The type of task, such as Call or Meeting."
    suggestions: ["Email","Other","Call"]
    sql: ${TABLE}.type ;;
  }

  measure: count {
    type: count
    drill_fields: [task_name, task_type,task_priority, task_status,]
  }
}


# dimension: recordtypeid {
#   type: string
#   description: "{\"dl\":\"512\"}"
#   sql: ${TABLE}.recordtypeid ;;
# }

# dimension_group: reminder_datetime {
#   type: time
#   description: "{\"dl\":\"13\"}"
#   timeframes: [
#     raw,
#     time,
#     date,
#     week,
#     month,
#     quarter,
#     year
#   ]
#   sql: ${TABLE}.reminderdatetime ;;
# }


# dimension: sales_navigator_action__c {
#   type: number
#   description: "{\"dl\":\"38\",\"scale\":\"9\"}"
#   sql: ${TABLE}.sales_navigator_action__c ;;
# }

  # dimension: gong__gong_selected_process__c {
  #   type: yesno
  #   description: "{\"dl\":\"5\"}"
  #   sql: ${TABLE}.gong__gong_selected_process__c ;;
  # }

  # dimension: hiring_manager__c {
  #   type: string
  #   description: "{\"dl\":\"512\"}"
  #   sql: ${TABLE}.hiring_manager__c ;;
  # }


  # dimension: latest_account_activity__c {
  #   type: yesno
  #   description: "{\"dl\":\"5\"}"
  #   sql: ${TABLE}.latest_account_activity__c ;;
  # }

  # dimension_group: meeting_date__c {
  #   type: time
  #   description: "{\"dl\":\"10\"}"
  #   timeframes: [
  #     raw,
  #     date,
  #     week,
  #     month,
  #     quarter,
  #     year
  #   ]
  #   convert_tz: no
  #   datatype: date
  #   sql: ${TABLE}.meeting_date__c ;;
  # }

  # dimension: outreach_general_action__c {
  #   type: string
  #   description: "{\"dl\":\"512\"}"
  #   sql: ${TABLE}.outreach_general_action__c ;;
  # }

  # dimension: outreach_linkedin_action__c {
  #   type: string
  #   description: "{\"dl\":\"512\"}"
  #   sql: ${TABLE}.outreach_linkedin_action__c ;;
  # }

  # dimension: outreach_task_status__c {
  #   type: string
  #   description: "{\"dl\":\"512\"}"
  #   sql: ${TABLE}.outreach_task_status__c ;;
  # }

  # dimension: was_replied__c {
  #   type: yesno
  #   description: "{\"dl\":\"5\"}"
  #   sql: ${TABLE}.was_replied__c ;;
  # }

  # dimension: was_viewed__c {
  #   type: yesno
  #   description: "{\"dl\":\"5\"}"
  #   sql: ${TABLE}.was_viewed__c ;;
  # }

  # dimension: whatid {
  #   type: string
  #   description: "{\"dl\":\"512\"}"
  #   sql: ${TABLE}.whatid ;;
  # }

  # dimension: whoid {
  #   type: string
  #   description: "{\"dl\":\"512\"}"
  #   sql: ${TABLE}.whoid ;;
  # }

# dimension: salesloft_bounced__c {
#   type: yesno
#   description: "{\"dl\":\"5\"}"
#   sql: ${TABLE}.salesloft_bounced__c ;;
# }

# dimension: salesloft_cadence_name__c {
#   type: string
#   description: "{\"dl\":\"512\"}"
#   sql: ${TABLE}.salesloft_cadence_name__c ;;
# }

# dimension_group: salesloft_call_end_time__c {
#   type: time
#   description: "{\"dl\":\"13\"}"
#   timeframes: [
#     raw,
#     time,
#     date,
#     week,
#     month,
#     quarter,
#     year
#   ]
#   sql: ${TABLE}.salesloft_call_end_time__c ;;
# }

# dimension: salesloft_call_to__c {
#   type: string
#   description: "{\"dl\":\"512\"}"
#   sql: ${TABLE}.salesloft_call_to__c ;;
# }

# dimension_group: salesloft_canceled_date__c {
#   type: time
#   description: "{\"dl\":\"13\"}"
#   timeframes: [
#     raw,
#     time,
#     date,
#     week,
#     month,
#     quarter,
#     year
#   ]
#   sql: ${TABLE}.salesloft_canceled_date__c ;;
# }

# dimension: salesloft_clicked_count__c {
#   type: number
#   description: "{\"dl\":\"38\",\"scale\":\"9\"}"
#   sql: ${TABLE}.salesloft_clicked_count__c ;;
# }

# dimension: salesloft_email_template_title__c {
#   type: string
#   description: "{\"dl\":\"512\"}"
#   sql: ${TABLE}.salesloft_email_template_title__c ;;
# }

# dimension: salesloft_meeting_assigned_to__c {
#   type: string
#   description: "{\"dl\":\"512\"}"
#   sql: ${TABLE}.salesloft_meeting_assigned_to__c ;;
# }

# dimension: salesloft_meeting_booked_by__c {
#   type: string
#   description: "{\"dl\":\"512\"}"
#   sql: ${TABLE}.salesloft_meeting_booked_by__c ;;
# }

# dimension: salesloft_no_show__c {
#   type: yesno
#   description: "{\"dl\":\"5\"}"
#   sql: ${TABLE}.salesloft_no_show__c ;;
# }

# dimension: salesloft_sentiment__c {
#   type: string
#   description: "{\"dl\":\"512\"}"
#   sql: ${TABLE}.salesloft_sentiment__c ;;
# }

# dimension_group: salesloft_start_time__c {
#   type: time
#   description: "{\"dl\":\"13\"}"
#   timeframes: [
#     raw,
#     time,
#     date,
#     week,
#     month,
#     quarter,
#     year
#   ]
#   sql: ${TABLE}.salesloft_start_time__c ;;
# }

# dimension: salesloft_step_type__c {
#   type: string
#   description: "{\"dl\":\"512\"}"
#   sql: ${TABLE}.salesloft_step_type__c ;;
# }

# dimension: salesloft_viewed_count__c {
#   type: number
#   description: "{\"dl\":\"38\",\"scale\":\"9\"}"
#   sql: ${TABLE}.salesloft_viewed_count__c ;;
# }

# dimension: salesloftreplycount__c {
#   type: number
#   description: "{\"dl\":\"38\",\"scale\":\"9\"}"
#   sql: ${TABLE}.salesloftreplycount__c ;;
# }
