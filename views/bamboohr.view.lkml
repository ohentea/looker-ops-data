view: bamboohr {
  view_label: "BambooHR"
  sql_table_name: `ptf-operations-analytics.hevo_dataset_ptf_operations_analytics_0SNh.bamboohr_api`
    ;;
  drill_fields: [employee_id]
## API Field names https://documentation.bamboohr.com/docs/list-of-field-names
## Hevo pipeline (custom API call) https://us.hevodata.com/pipeline/64/overview


  dimension: employee_id {
    primary_key: yes
    type: string
    description: "The employee ID automatically assigned by BambooHR."
    sql: ${TABLE}.id ;;
  }

  dimension: name_first {
    type: string
    hidden: yes
    description: "  The employee's first name."
    sql: ${TABLE}.firstname ;;
  }

  dimension: name_last {
    type: string
    hidden: yes
    description: "The employee's last name."
    sql: ${TABLE}.lastname ;;
  }

  dimension: name {
    type: string
    hidden: yes
    description: "The employee's full name."
    sql: ${name_first} || " " || ${name_last} ;;
  }

  dimension: gender {
    type: string
    description: ""
    suggestions: ["Female","Male","Non-binary"]
    sql: ${TABLE}.gender ;;
  }

  dimension: gender_identity {
    type: string
    description: "{\"dl\":\"512\"}"
    sql: ${TABLE}.genderidentity ;;
  }

  dimension: ethnicity {
    type: string
    description: "The employee's ethnicity."
    suggestions: ["Asian", "Black or African American", "Hispanic or Latino", "Two or More Races", "White"]
    sql: ${TABLE}.ethnicity ;;
  }


  dimension: department {
    type: string
    suggestions: ["DEIB", "General & Admin", "Marketing", "Product", "Sales", "Staff Augmentation", "Talent Acquisition", "Tech & Development"]
    sql: ${TABLE}.department ;;
  }

  dimension: department_custom {
    type: string
    description: "More granular department information"
    sql: ${TABLE}.customsub_department ;;
  }


  dimension: city {
    type: string
    description: ""
    group_label: "Location"
    sql: ${TABLE}.city ;;
  }

  dimension: state {
    type: string
    description: ""
    group_label: "Location"
    sql: ${TABLE}.state ;;
  }

  dimension: country {
    type: string
    description: ""
    group_label: "Location"
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension_group: status {
    type: time
    timeframes: [date]
    sql: ${TABLE}.employeestatusdate ;;
  }

  dimension: status_detail {
    type: string
    description: "Returns terminated if applicable, or Contractor/Freelancer/Full-Time"
    suggestions: ["Terminated", "Staff Aug - Contractor (Full-Time)", "Vendor/Third-Party Agency (Full-Time)", "Consultant/Freelancer", "Full-Time (US)", "Contractor (Full-Time)"]
    sql: ${TABLE}.employmenthistorystatus ;;
  }

  dimension: supervisor {
    type: string
    description: "Name of supervisor"
    sql: ${TABLE}.hevo_91 ;;
  }

  dimension_group: hire {
    type: time
    timeframes: [date, month, quarter]
    description: "The date the employee was hired."
    sql: DATE(${TABLE}.hiredate) ;;
  }

  dimension: job_title {
    type: string
    description: "The CURRENT value of the employee's job title, updating this field will create a new row in position history."
    sql: ${TABLE}.jobtitle ;;
    suggestions: ["Staff Aug Resource", "Global DEIB Strategist and Trainer", "Talent Advocate", "Freelance Video Editor", "Account Executive", "Marketing Consultant"]
  }


  dimension: location {
    type: string
    description: "The employee's CURRENT location."
    group_label: "Location"
    sql: ${TABLE}.location ;;
  }


  dimension: status {
    type: string
    description: "The employee's employee status (Active or Inactive)."
    sql: ${TABLE}.status ;;
  }

  dimension: work_email {
    type: string
    hidden: yes
    description: "The employee's work email address."
    sql: ${TABLE}.workemail ;;
  }

  measure: count {
    type: count
    drill_fields: [employee_id, name]
  }
}
